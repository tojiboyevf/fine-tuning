
import sys
import torch
import logging
import numpy as np
from tqdm import tqdm
import multiprocessing
from datetime import datetime
import torchvision.transforms as T
import pandas as pd
torch.backends.cudnn.benchmark= True  # Provides a speedup
import os

import json
import parser
import cosface_loss
import util
from model import network
from datasets.train_dataset import TDataset
from torch.utils.data import DataLoader
import util 
import commons

args = parser.parse_arguments(True)
start_time = datetime.now()
output_folder = f"logs/{args.save_dir}/{start_time.strftime('%Y-%m-%d_%H-%M-%S')}"
commons.make_deterministic(args.seed)
commons.setup_logging(output_folder, console="debug")
logging.info(" ".join(sys.argv))
logging.info(f"Arguments: {args}")
logging.info(f"The outputs are being saved in {output_folder}")

#### Model
model = network.GeoLocalizationNet(args.backbone, args.fc_output_dim)

logging.info(f"There are {torch.cuda.device_count()} GPUs and {multiprocessing.cpu_count()} CPUs.")

if args.resume_model != None:
    logging.debug(f"Loading model from {args.resume_model}")
    model_state_dict = torch.load(args.resume_model)
    model.load_state_dict(model_state_dict)

model = model.to(args.device).train()

#### Optimizer
criterion = torch.nn.CrossEntropyLoss()
model_optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)

#### Datasets
associations = pd.read_csv(os.path.join(args.train_set_folder, args.associations_file), sep=' ', header=None).drop(1, axis=1)
associations.columns =['rgb', 'class']

with open(os.path.join(args.dataset_folder, args.groups_file)) as f:
   groups_and_classes = json.load(f)
# groups_and_classes = create_groups_and_classes() # Future improvements
resize = args.resize
train_transform = T.Compose([
                    T.ColorJitter(brightness=args.brightness,
                                  contrast=args.contrast,
                                  saturation=args.saturation,
                                  hue=args.hue),
                    T.RandomResizedCrop([resize[0], resize[1]], scale=[1-args.random_resized_crop, 1]),
                    T.ToTensor(),
                    T.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                ])

groups = [
            TDataset(root = args.train_set_folder, 
            associations = associations.loc[associations['class'].isin(group)].to_numpy(),
            transform = train_transform) for group in groups_and_classes
        ]
# Each group has its own classifier, which depends on the number of classes in the group
classifiers = [cosface_loss.MarginCosineProduct(args.fc_output_dim, len(group)) for group in groups]
classifiers_optimizers = [torch.optim.Adam(classifier.parameters(), lr=args.classifiers_lr) for classifier in classifiers]

logging.info(f"Using {len(groups)} groups")
logging.info(f"The {len(groups)} groups have respectively the following number of classes {[len(g) for g in groups_and_classes]}")
logging.info(f"The {len(groups)} groups have respectively the following number of images {[len(g) for g in groups]}")

#### Resume
if args.resume_train:
    model, model_optimizer, classifiers, classifiers_optimizers, best_val_recall1, start_epoch_num = \
        util.resume_train(args, output_folder, model, model_optimizer, classifiers, classifiers_optimizers)
    model = model.to(args.device)
    epoch_num = start_epoch_num - 1
    logging.info(f"Resuming from epoch {start_epoch_num} with best R@1 {best_val_recall1:.1f} from checkpoint {args.resume_train}")
else:
    start_epoch_num = 0

#### Train / evaluation loop
logging.info("Start training ...")
logging.info(f"There are {len(groups[0])} classes for the first group, " +
             f"each epoch has {args.iterations_per_epoch} iterations " +
             f"with batch_size {args.batch_size}, therefore the model sees each class (on average) " +
             f"{args.iterations_per_epoch * args.batch_size / len(groups[0]):.1f} times per epoch")

for epoch_num in range(start_epoch_num, args.epochs_num):
    
    #### Train
    epoch_start_time = datetime.now()
    current_group_num = epoch_num % len(groups_and_classes)
    classifiers[current_group_num] = classifiers[current_group_num].to(args.device)
    util.move_to_device(classifiers_optimizers[current_group_num], args.device)
    
    dataloader = DataLoader(groups[current_group_num], num_workers=args.num_workers,
                                            batch_size=args.batch_size, shuffle=True,
                                            pin_memory=(args.device=="cuda"), drop_last=True)
    
    model = model.train()
    epoch_losses = np.zeros((0,1), dtype=np.float32)
    total = 0
    correct = 0
    best_acc = 0

    for batch_idx, batch in enumerate(tqdm(iter(dataloader), ncols=100)):
        images, targets = batch
        images, targets = images.to(args.device), targets.to(args.device)
        
        model_optimizer.zero_grad()
        classifiers_optimizers[current_group_num].zero_grad()
        
        descriptors = model(images)
        output = classifiers[current_group_num](descriptors, targets)
        loss = criterion(output, targets)
        loss.backward()
        epoch_losses = np.append(epoch_losses, loss.item())
        model_optimizer.step()
        classifiers_optimizers[current_group_num].step()
        
        _, y_pred = output.max(1)
        total += targets.size(0)
        correct += y_pred.eq(targets).sum().item()

        del loss, images, output
    
    train_acc = correct / total
    
    classifiers[current_group_num] = classifiers[current_group_num].cpu()
    util.move_to_device(classifiers_optimizers[current_group_num], "cpu")
    
    logging.debug(f"Epoch {epoch_num:02d} in {str(datetime.now() - epoch_start_time)[:-7]}, "
                 f"loss = {epoch_losses.mean():.4f}")
    
    #### Saving checkpoint
    logging.info(f"Epoch {epoch_num:02d} in {str(datetime.now() - epoch_start_time)[:-7]}, train_acc : {train_acc * 100 :.2f} train_loss : {epoch_losses.mean():.4f}")
    is_best = train_acc > best_acc
    best_acc = max(best_acc, train_acc)
    util.save_checkpoint({"epoch_num": epoch_num + 1,
        "model_state_dict": model.state_dict(),
        "optimizer_state_dict": model_optimizer.state_dict(),
        "classifiers_state_dict": [c.state_dict() for c in classifiers],
        "optimizers_state_dict": [c.state_dict() for c in classifiers_optimizers],
        "best_acc" : best_acc
    }, is_best, output_folder)


logging.info(f"Trained for {epoch_num+1:02d} epochs, in total in {str(datetime.now() - start_time)[:-7]}")

logging.info("Experiment finished (without any errors)")